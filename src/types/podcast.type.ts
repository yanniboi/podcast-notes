export default interface IPodcastData {
  type: string,
  id: number,
  attributes: IPodcastAttributes,
}

export interface IPodcastAttributes {
  title: string,
  drupal_internal__nid: number,
  status: boolean,
  created: string,
  changed: string,
  field_mp3: string,
}