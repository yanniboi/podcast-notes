export default interface INoteData {
  type: string,
  id: string,
  attributes: INoteAttributes,
  relationships: INoteRelationships,
}

export interface INoteRequestData {
  data: INoteData
}

export interface INoteRelationships {
  user_id: IUserData,
}

export interface IUserData {
  data: IUserDataProperties,
}

export interface IUserDataProperties {
  type: string,
  id: string,
}

export interface INoteAttributes {
  drupal_internal__id: number,
  note: IProcessedText,
  status: boolean,
  progress: string,
  entity_id: number,
  entity_type_id: string,
  created: string,
  changed: string,
}

export interface IProcessedText {
  value: string,
  format: string,
  processed: string,
}