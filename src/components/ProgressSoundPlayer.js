import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withSoundCloudAudio } from 'react-soundplayer/addons';
import { PlayButton, Progress, VolumeControl, Timer } from 'react-soundplayer/components';
import Spinner from './atoms/Spinner'

export const NextIconSVG = () => (
  <svg
    className="sb-soundplayer-icon"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    fill="currentColor"
  >
    <path d="M4 4 L24 14 V4 H28 V28 H24 V18 L4 28 z " />
  </svg>
);

class ProgressSoundPlayer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      playerLoaded: false,
      initProgress: false,
      loading: true,
    };
  }

  // Set to true to enable console logging.
  debug = false;

  componentDidMount() {
    const {soundCloudAudio} = this.props;

    // Add additional tracking events
    soundCloudAudio.on('loadeddata', this.onLoaded.bind(this));
    soundCloudAudio.on('seeked', this.onSeeked.bind(this));
    soundCloudAudio.on('seeking', this.onSeeking.bind(this));
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.initialProgress(prevProps);
    if (this.props.needsSeek) {
      this.handleSeek(prevProps);
    }
  }

  initialProgress(prevProps) {
    if (this.state.initProgress || !this.state.playerLoaded) {
      return;
    }

    const {startProgress, soundCloudAudio} = this.props;

    soundCloudAudio.audio.currentTime = startProgress;

    this.setState({
      initProgress: true,
    })
  }

  handleSeek(prevProps) {
    if (this.props.seeking) {
      return;
    }

    if (!this.state.initProgress || !this.state.playerLoaded) {
      return;
    }

    const {startProgress, soundCloudAudio} = this.props;
    soundCloudAudio.audio.currentTime = startProgress;
  }

  /**
   * Track when the track is loaded so we know we can update progress.
   */
  onLoaded() {
    this.setState({
      playerLoaded: true,
    })
  }

  /**
   * Track when the track is has finished seeking to a new position.
   */
  onSeeked() {
    const {soundCloudAudio, onSeekedTrack} = this.props;
    onSeekedTrack(soundCloudAudio);

    this.setState({
      loading: false,
    })
  }
  onSeeking() {
    this.setState({
      loading: true,
    })
  }

  render() {
    const { episode } = this.props;
    const { loading } = this.state;
    return (
      <>
        {loading && (
          <Spinner />
        )}

        {!loading && (
          <>
            <div className="p2 border navy mt1 mb3 flex flex-center rounded">
              <PlayButton className="flex-none h4 mr2 button white btn-big button-outline button-grow bg-orange circle"
                          seekingIcon={<NextIconSVG />}
                          {...this.props} />
              <div className="flex-auto">
                {/*<strong className="h4 nowrap m0">Zackavelli</strong>*/}
                <h2 className="h4 nowrap caps m0">{episode.attributes.title}</h2>
                <div className='flex flex-center'>
                  <VolumeControl
                    className='mr2 flex flex-center'
                    buttonClassName="flex-none h6 button white btn-small button-outline button-grow bg-orange circle btn-square"
                    {...this.props} />
                  <Progress
                    className="mt1 mb1 mr2 rounded"
                    innerClassName="rounded-left"
                    {...this.props} />
                  <Timer
                    className="h6 mr1"
                    {...this.props} />
                </div>
              </div>
            </div>
          </>
        )}
      </>
    );
  }
}

ProgressSoundPlayer.propTypes = {
  streamUrl: PropTypes.string.isRequired,
};

export default withSoundCloudAudio(ProgressSoundPlayer);