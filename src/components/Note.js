import {MdDeleteForever, MdPlaylistPlay, MdReplay} from 'react-icons/md';

const Note = ({ id, text, date, progress, handleDeleteNote, handleSeekTrack }) => {

  const progressAsTime = (progress) => {
    const minutes = pad(Math.floor(progress / 60), 2);
    const seconds = pad(Math.floor(progress - minutes * 60),2);
    return `${minutes}:${seconds}`;
  }

  const pad = (num, size) => {
    num = num.toString();
    while (num.length < size) num = "0" + num;
    return num;
  }

  return (
    <div className='note'>
      <span>{text}</span>
      <div className='note-footer'>
        <small>{date}</small>
        <div className='time'>
          <MdPlaylistPlay
            onClick={() => handleSeekTrack(progress)}
            className='seek-icon'
            size='1.3em'
          />
          <small>{progressAsTime(progress)}</small>
        </div>

        <MdDeleteForever
          onClick={() => handleDeleteNote(id)}
          className='delete-icon'
          size='1.3em'
        />
      </div>
    </div>
  );
};

export default Note;