import { useState } from 'react';
import {MdPlaylistAdd} from "react-icons/md";

const AddNote = ({ currentProgress, handleAddNote }) => {
  const [noteText, setNoteText] = useState('');
  const characterLimit = 200;

  const handleChange = (event) => {
    if (characterLimit - event.target.value.length >= 0) {
      setNoteText(event.target.value);
    }
  };

  const handleSaveClick = () => {
    if (noteText.trim().length > 0) {
      handleAddNote(noteText);
      setNoteText('');
    }
  };

  const progressAsTime = (progress) => {
    const minutes = pad(Math.floor(progress / 60), 2);
    const seconds = pad(Math.floor(progress - minutes * 60),2);
    return `${minutes}:${seconds}`;
  }

  const pad = (num, size) => {
    num = num.toString();
    while (num.length < size) num = "0" + num;
    return num;
  }

  return (
    <div className='note new'>
			<textarea
        rows='8'
        cols='10'
        placeholder='Type to add a note...'
        value={noteText}
        onChange={handleChange}
      ></textarea>
      <div className='note-footer'>
        <small>
          {characterLimit - noteText.length} Remaining
        </small>
        <div className='time'>
          <MdPlaylistAdd
            className='time-icon'
            size='1.3em'
          />
          <small>{progressAsTime(currentProgress)}</small>
        </div>
        <button className='save' onClick={handleSaveClick}>
          Save
        </button>
      </div>
    </div>
  );
};

export default AddNote;