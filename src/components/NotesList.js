import Note from './Note';
import AddNote from './AddNote';
import {Component} from "react";

class NotesList extends Component {

  render() {
    const {notes, currentProgress, handleAddNote, handleDeleteNote, handleSeekTrack} = this.props;

    return (
      <div className='notes-list'>
        {notes.map((note) => (
          <Note
            key={note.id}
            id={note.id}
            text={note.text}
            date={note.date}
            handleDeleteNote={handleDeleteNote}
            progress={note.progress}
            handleSeekTrack={handleSeekTrack}
          />
        ))}
        <AddNote
          handleAddNote={handleAddNote}
          currentProgress={currentProgress}
        />
      </div>
    );
  }
};

export default NotesList;