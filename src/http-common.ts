import axios, {AxiosInstance} from "axios";
class ApiClient {

  client: AxiosInstance

  constructor(url: string) {
    this.client = axios.create({
      baseURL: `${url}/jsonapi/`,
      headers: {
        "Content-type": "application/vnd.api+json",
        "Accept": "application/vnd.api+json",
      }
    });
  }
}

export default ApiClient;