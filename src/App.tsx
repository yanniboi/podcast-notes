import {useEffect, useState} from 'react';
import {nanoid} from 'nanoid';
import NotesList from './components/NotesList';
import Search from './components/Search';
import Header from './components/Header';
import PodcastDataService from "./services/podcast.service";
import NoteDataService from "./services/note.service";
import ProgressSoundPlayer from "./components/ProgressSoundPlayer";
import "./styles/player.scss"
import IPodcastData from "./types/podcast.type";
import INoteData, {INoteRequestData} from "./types/note.type";
import drupalSettings from "./services/drupalSettings.service";
import Spinner from "./components/atoms/Spinner"
import moment from 'moment';

interface INote {
  id: string;
  text: string;
  date: string;
  progress: number;
}

const App = () => {

  // Set to true to enable console logging.
  const debug = false;

  const [client, setClient] = useState<NoteDataService>();
  const [episode, setEpisode] = useState<IPodcastData>();
  const [notes, setNotes] = useState<INote[]>([]);
  const [progress, setProgress] = useState(0);
  const [seek, setSeek] = useState(false);
  const [loading, setLoading] = useState(true);

  const [searchText, setSearchText] = useState('');

  const loadPodcastEpisode = () => {
    new PodcastDataService(drupalSettings.podcast_app.url).get(drupalSettings.podcast_app.episode_uuid)
      .then((response: any) => {
        setEpisode(response.data.data);
      });
  }

  const loadPodcastNotes = () => {
    if (episode === undefined) {
      return;
    }

    if (drupalSettings.podcast_app.username === "") {
      return;
    }

    client?.getByEpisode(episode.attributes.drupal_internal__nid)
      .then((response: any) => {
        debug && console.log(response, 'get all notes response');
        setNotes(buildNotesFromApi(response.data.data));
      })
      .catch(err => console.log(err));
  }

  const buildNotesFromApi = (apiNotes: INoteData[]) => {
    debug && console.log(apiNotes, 'notes from api');
    // let notes: INote[] = apiNotes.map(apiNote => {
    let notes: any = apiNotes.map(apiNote => {
      return {
        id: apiNote.id,
        text: apiNote.attributes.note.value,
        date: moment(apiNote.attributes.created).format('D/M/Y HH:mm'),
        progress: apiNote.attributes.progress,
      }
    });
    return notes;
  }

  /**
   * On mount load saved data from local storage.
   */
  useEffect(() => {
    loadPodcastEpisode();
  }, []);

  useEffect(() => {
    if (episode === undefined) {
      return;
    }

    const savedNotes = JSON.parse(
      localStorage.getItem(`react-notes-app-${episode?.id}-data`) || '[]'
    );
    if (savedNotes) {
      setNotes(savedNotes);
    }

    const savedProgress = JSON.parse(
      localStorage.getItem(`react-notes-app-${episode?.id}-progress`) || '0'
    );
    if (savedProgress) {
      setProgress(savedProgress);
    }

    setLoading(false);

    const {url, csrf, username} = drupalSettings.podcast_app;
    const client = new NoteDataService(url, username, csrf);
    setClient(client);
  }, [episode]);

  /**
   * Store notes data when notes are updated.
   */
  useEffect(() => {
    // For some reason on mount the initial load of notes doesn't work :shrug:
    // Therefore only set the local storage if we don't have the default state.
    if (notes.length === 0) {
      return;
    }

    localStorage.setItem(
      `react-notes-app-${episode?.id}-data`,
      JSON.stringify(notes)
    );
  }, [notes]);

  useEffect(() => {
    if (client === undefined) {
      return;
    }
    loadPodcastNotes();
  }, [client]);

  /**
   * Store track progress when play/pause/end happens.
   */
  useEffect(() => {
    if (progress === 0) {
      return;
    }

    let newProgress = progress;

    if (progress === -1) {
      newProgress = 0
    }

    localStorage.setItem(
      `react-notes-app-${episode?.id}-progress`,
      JSON.stringify(newProgress)
    );
  }, [progress]);

  /**
   * Store track progress when play/pause/end happens.
   */
  useEffect(() => {
    if (seek === true) {
      setSeek(false);
    }
  }, [seek]);

  const addNote = (text: string) => {
    const date = new Date();
    const newNote = {
      id: nanoid(),
      text: text,
      date: moment().format('D/M/Y HH:mm'),
      progress: progress,
    };
    const newNotes = [...notes, newNote];
    setNotes(newNotes);

    if (drupalSettings.podcast_app.username === "") {
      return;
    }

    const note = {
      data: {
        type: "tgd_note--tgd_note",
        relationships: {
          user_id: {
            data: {
              type: 'user--user',
              id: drupalSettings.podcast_app.user_id,
            }
          }
        },
        attributes: {
          entity_id: episode?.attributes.drupal_internal__nid,
          entity_type_id: "node",
          progress: newNote.progress.toString(),
          note: {
            value: newNote.text,
            format: "plain_text"
          }
        }
      }
    } as INoteRequestData;
    client?.create(note);
  };

  const deleteNote = (id: string) => {
    const newNotes = notes.filter((note) => note.id !== id);
    setNotes(newNotes);

    if (drupalSettings.podcast_app.username === "") {
      return;
    }

    client?.delete(id,);
  };

  const seekNote = (progress: number) => {
    debug && console.log(progress);
    setProgress(progress)
    setSeek(true)
  };

  /**
   * Store progress when play starts.
   */
  const onStart = (soundCloudAudio: any) => {
    debug && console.log('start');

    setProgress(soundCloudAudio.audio.currentTime)
  }

  /**
   * Clear saved progress when track ends.
   */
  const onStop = (soundCloudAudio: any) => {
    debug && console.log('stop');

    setProgress(-1)
  }

  /**
   * Store progress when track is paused.
   */
  const onPause = (soundCloudAudio: any) => {
    debug && console.log('pause');

    setProgress(soundCloudAudio.audio.currentTime)
  }

  /**
   * Store progress when track is finished seeking.
   */
  const onSeeked = (soundCloudAudio: any) => {
    debug && console.log('seeked');

    setProgress(soundCloudAudio.audio.currentTime)
  }

  // @fixme When is this even called?
  const onCanPlay = (soundCloudAudio: any) => {
    debug && console.log('can play');
  }

  return (
    <div className='container'>
      {loading && (
        <Spinner />
      )}

      {!loading && (
        <>
          <Header title="Listen"/>
          {episode && (
            <ProgressSoundPlayer
              episode={episode}
              streamUrl={episode?.attributes.field_mp3}
              onStartTrack={onStart}
              onStopTrack={onStop}
              onPauseTrack={onPause}
              onSeekedTrack={onSeeked}
              onCanPlay={onCanPlay}
              startProgress={progress}
              needsSeek={seek}
            />
          )}

          <Header title="Notes"/>
          <Search handleSearchNote={setSearchText}/>
          <NotesList
            notes={notes.filter((note) =>
              note.text.toLowerCase().includes(searchText)
            )}
            currentProgress={progress}
            handleAddNote={addNote}
            handleDeleteNote={deleteNote}
            handleSeekTrack={seekNote}
          />
        </>
      )}
    </div>
  );
};

export default App;
