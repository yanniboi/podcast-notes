import ApiClient from "../http-common";
import IPodcastData from "../types/podcast.type"
import {AxiosInstance} from "axios";
class PodcastDataService {

  url: string
  client: AxiosInstance

  constructor(url: string) {
    this.url = url;
    this.client = new ApiClient(this.url).client;
  }

  getAll() {
    return this.client.get<Array<IPodcastData>>("/node/podcast");
  }
  get(id: string) {
    return this.client.get<IPodcastData>(`/node/podcast/${id}`);
  }
  create(data: IPodcastData) {
    return this.client.post<IPodcastData>("/node/podcast", data);
  }
  update(data: IPodcastData, id: any) {
    return this.client.put<any>(`/node/podcast/${id}`, data);
  }
  delete(id: any) {
    return this.client.delete<any>(`/node/podcast/${id}`);
  }
}
export default PodcastDataService;