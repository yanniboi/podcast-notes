class DrupalSettings {
  podcast_app: any;

  constructor() {
    if ((<any>window).drupalSettings !== undefined) {
      Object.assign(this, (<any>window).drupalSettings);
    } else {
      // Modify this object to provide credentials when not running within Drupal site.
      const defaultSettings = {
        podcast_app: {
          episode_uuid: "4a6de4df-bc53-4aa8-86be-862649b31df4",
          user_id: "e1d81ab8-1c29-4e56-9122-fefb6037ae05",
          url: "http://gdfg.ddev.site",
          username: "yanniboi",
          csrf: "E7XZBiEOevgI5BwsBIdk1eUubR4aZnpKBhbSkjGoDfE",
        }
      };

      Object.assign(this, defaultSettings);
    }
  }
}

export default new DrupalSettings();