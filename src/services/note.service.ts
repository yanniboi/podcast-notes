import ApiClient from "../http-common";
import INoteData, {INoteRequestData} from "../types/note.type"
import {AxiosInstance} from "axios";
class NoteDataService {

  url: string
  client: AxiosInstance
  config: any

  constructor(url: string, username: string, token: string) {
    this.url = url;
    this.client = new ApiClient(this.url).client;
    this.config = {
      auth: {
        username: username,
        password: 'podcast_app',
      },
      headers: {
        'X-CSRF-Token': token,
      }
    };
  }

  getAll() {
    return this.client.get<Array<INoteData>>("/tgd_note/tgd_note", this.config);
  }
  getByEpisode(id: number) {
    return this.client.get<Array<INoteData>>("/tgd_note/tgd_note?filter[entity_id]="+id, this.config);
  }
  get(id: string) {
    return this.client.get<INoteData>(`/tgd_note/tgd_note/${id}`);
  }
  create(data: INoteRequestData) {
    return this.client.post<INoteRequestData>("/tgd_note/tgd_note", data, this.config);
  }
  update(data: INoteData, id: any) {
    return this.client.put<any>(`/tgd_note/tgd_note/${id}`, data);
  }
  delete(id: any,) {
    return this.client.delete<any>(`/tgd_note/tgd_note/${id}`, this.config);
  }
}
export default NoteDataService;